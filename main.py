#!/usr/bin/python

import sys, getopt, os, argparse

from src.process import process
from src.cleaner import clean
from src.configurator import getDefaultConfigurationFilename, loadConfigurationFile

def main(argv):
    
    _curent_working_dir = os.getcwd()

    parser = argparse.ArgumentParser()
    parser.add_argument("--configuration", dest="configuration_file", help="Configuration filename", metavar="FILE")
    parser.add_argument("--check-configuration-only", dest="check_configuration", help="Checks configuration file", action="store_true")
    parser.add_argument("--clean-all-only", dest="clean_all", help="Process clean of generation folder", action="store_true")
    parser.add_argument("--process-only", dest="process", help="Process generation of all files", action="store_true")
    args = parser.parse_args()

    configuration = dict()
    configuration_path_file = getDefaultConfigurationFilename()
    if (args.configuration_file != None):
        configuration_path_file = args.configuration_file

    try:
        configuration = loadConfigurationFile(configuration_path_file)
    except FileNotFoundError:
        print('Configuration file not found')
        sys.exit(0)
    except NameError as error:
        error_messages = {
            'InvalidConfiguration': 'Invalid configuration',
            'InvalidPathConfiguration': 'Invalid path in configuration, pathes must be relatives'
        }
        print(error_messages.get(error.args[0], "Unknown error"))
        sys.exit(0)

    if (args.check_configuration == True):
        print("Configuration file valid")
        sys.exit(0)

    if (args.clean_all == True):
        print("Cleaning")
        cleanAll(configuration)
        print("Clean done")
        sys.exit(0)

    if (args.process == True):
        print("Processing")
        process(configuration)
        print("Process done")
        sys.exit(0)

    print(_curent_working_dir)
    print(__file__)
    print(args)
    process(configuration)


if __name__ == "__main__":
   main(sys.argv[1:])
