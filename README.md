# Python comment parser

## Running the env

```bash
# build env
docker build -t dpython .

# run env
docker run -it -v `pwd`:/project dpython

# run env with default jekyll dev port
docker run -it -p 4000:4000 -v `pwd`:/project dpython

```

## Testing the project

```bash
# Just testing
py.test

# testing and let it show prints for debug
py.test -s
```

## Coverage of the project

```bash
# testing and coverage run
coverage run --source=src/ -m py.test

# viewing coverage report
coverage report
```


## Selected theme for starting
https://github.com/artemsheludko/flexible-jekyll


## Scripts outputs

3D rendered files are generated in /assets/previews/image_name.png

3D files to download are generated in /assets/models/stl_file_name.stl