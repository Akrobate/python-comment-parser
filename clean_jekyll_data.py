import os
from glob import glob

# Cleaning previews
source_path = './data/jekyll/assets/previews'
extension_filter = 'png'

result = glob(source_path + "/**/*." + extension_filter, recursive = True)

for file_name in result:
    os.remove(file_name)
    print(file_name + ' - removed')


# Cleaning stl_files
source_path = './data/jekyll/assets/stl_files'
extension_filter = 'stl'

result = glob(source_path + "/**/*." + extension_filter, recursive = True)

for file_name in result:
    os.remove(file_name)
    print(file_name + ' - removed')


# Cleaning pages
source_path = './data/jekyll/_pages'
extension_filter = 'markdown'

result = glob(source_path + "/**/*." + extension_filter, recursive = True)

for file_name in result:
    os.remove(file_name)
    print(file_name + ' - removed')
