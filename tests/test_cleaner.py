import os
from src.library import file_manager
from src import configurator

from src import cleaner

dir_path = os.path.dirname(os.path.realpath(__file__))
seeds_example_project_yaml_file = os.path.realpath(dir_path + '/seeds/example_project/.openscad-jekyll.yml')


def test_cleanAll(monkeypatch):

    mock_called = 0
    def file_manager_r(*args, **kwargs):
        nonlocal mock_called
        mock_called = mock_called + 1

    monkeypatch.setattr(file_manager, "removeFilesInFolder", file_manager_r)

    config = configurator.loadConfigurationFile(seeds_example_project_yaml_file)
    cleaner.cleanAll(config)
    assert mock_called == 3

