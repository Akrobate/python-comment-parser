import os
from src.library.file_manager import opendAndParseYamlFile

from src import configurator

dir_path = os.path.dirname(os.path.realpath(__file__))
seeds_example_project_yaml_file = os.path.realpath(dir_path + '/seeds/example_project/.openscad-jekyll.yml')


def test_loadConfigurationFile():
    result = configurator.loadConfigurationFile(seeds_example_project_yaml_file)
    assert result['version'] == 1.0


def test_validateConfiguration_no_version():
    configuration_example = opendAndParseYamlFile(seeds_example_project_yaml_file)
    del configuration_example['version']
    try:
        configurator.validateConfiguration(configuration_example)
    except NameError as error:
        assert error.args[0] == 'InvalidConfiguration'


def test_validateConfiguration_no_paths():
    configuration_example = opendAndParseYamlFile(seeds_example_project_yaml_file)
    del configuration_example['paths']
    try:
        configurator.validateConfiguration(configuration_example)
    except NameError as error:
        assert error.args[0] == 'InvalidConfiguration'


def test_validateConfiguration_no_output_folder_images():
    configuration_example = opendAndParseYamlFile(seeds_example_project_yaml_file)
    del configuration_example['paths']['output_folder_images']
    try:
        configurator.validateConfiguration(configuration_example)
    except NameError as error:
        assert error.args[0] == 'InvalidConfiguration'


def test_validateConfiguration_no_output_folder_stl():
    configuration_example = opendAndParseYamlFile(seeds_example_project_yaml_file)
    del configuration_example['paths']['output_folder_stl']
    try:
        configurator.validateConfiguration(configuration_example)
    except NameError as error:
        assert error.args[0] == 'InvalidConfiguration'
