# Testing deployement example script

Be aware this script is destinated to be executed from inside the docker container.
It creates some folders in the route of the system. All paths configured depends on this and are hardcoded as it just a helper to check all the deployement sequence

```sh

#!/bin/bash

mkdir /repository;
cd /repository;

git clone https://github.com/Akrobate/hexapod-robot.git;
cd hexapod-robot;

git clone https://framagit.org/Akrobate/jekyll-openscad-theme.git
mv jekyll-openscad-theme jekyll

```