#!/bin/bash

mkdir /repository;
cd /repository;

git clone https://github.com/Akrobate/hexapod-robot.git;
cd hexapod-robot;

git clone https://framagit.org/Akrobate/jekyll-openscad-theme.git
mv jekyll-openscad-theme jekyll

cp /project/tests/example_script_test/.openscad-jekyll.yml /repository/hexapod-robot/

read -p "Process OSJ Build? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
cd /repository/hexapod-robot/
osj

read -p "Complile Jekyll and start? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
cd /repository/hexapod-robot/jekyll/
bundle install
bundler exec jekyll serve
