import os
from src.library import structure_tree

# Preparing SEEDS for tests

dir_path = os.path.dirname(os.path.realpath(__file__))
seeds_example_project_path = os.path.realpath(dir_path + '/../seeds/example_project')

generic_tree_seed = {
    'name': 'root',
    'children': [
        {
            'name': 'a'
        },
        {
            'name': 'b'
        },
        {
            'name': 'c',
            'children': [
                {
                    'name': 'ca'
                },
                {
                    'name': 'cb',
                    'children': [
                        {
                            'name': 'cba'
                        }
                    ]
                }
            ]
        },
        {
            'name': 'd'
        },
    ]
}

# Debug function declaration

def dg_printList(l):
    for i in l:
        print(i)


def dg_printTree(tree):
    def recursiveDisplayTree(branch, deep):
        print(deep + branch['name'])
        if 'children' in branch:
            for sub_branch in branch['children']:
                recursiveDisplayTree(sub_branch, '-' + deep)
    
    return recursiveDisplayTree(tree, '')


# Tests functions

def test_generateContentData():
    result = structure_tree.generateContentData(seeds_example_project_path)
    # dg_printList(result)
    assert len(result) == 11


def test_generateTree():
    content = structure_tree.generateContentData(seeds_example_project_path)
    result = structure_tree.generateTree(content)
    # print (result)
    # dg_printTree(result)
    node_leg = structure_tree.findNodeInTree(result, 'LegComponent')
    node_hip = structure_tree.findNodeInTree(result, 'HipComponent')
    assert node_leg != None
    assert node_hip != None
    assert len(node_hip['children']) == 4
    assert len(node_leg['children']) == 4


def test_findNodeInTree():
    # dg_printTree(generic_tree_seed)
    node = structure_tree.findNodeInTree(generic_tree_seed, 'cb')
    assert node == generic_tree_seed['children'][2]['children'][1]


def test_generateContentListWithFirstLevelChildren():
    content = structure_tree.generateContentData(seeds_example_project_path)
    result = structure_tree.generateContentListWithFirstLevelChildren(content)

    found_element = False
    for item in result:
        if item['name'] == 'HipComponent':
            found_element = item

    assert found_element != False
    assert 'children' in found_element
    assert len(found_element['children']) == 4
    assert not 'children' in found_element['children'][0]



