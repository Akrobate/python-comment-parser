from src.library import parser

comment_seed = '''
use <../envelopes/servo-arm-envelope.scad>
use <../assets/fonts/Freshman.ttf>

legPieceA();

/**
 * LegPieceA
 * @description Hold servo arm to the leg
 * @category leg
 * @tag piece
 * @stl
 */
module legPieceA(
    length = 50,
    width = 6,
    name = "A"
) {
    thickness = 3;

 '''

comment_seed_spaced = '''

/**
 * @description Hold servo arm to the leg
 *
 * @category leg
 *
 * @type piece
 *
 */

 '''

comment_seed_multiple = '''
/**
 * @description Hold servo arm to the leg
 *
 * @category leg
 *
 * @type piece
 *
 */

/**
 * @descriptionother Hold servo arm to the leg
 *
 * @categoryother leg
 *
 * @typeother piece
 *
 */
 '''

def test_parse():
    result = parser.parse(comment_seed)
    assert result == {
        'description': 'Hold servo arm to the leg',
        'category': 'leg',
        'tag': 'piece',
        'stl': '',
    }


def test_parse_spaced_values():
    result = parser.parse(comment_seed_spaced)
    assert result == {
        'description': 'Hold servo arm to the leg',
        'category': 'leg',
        'type': 'piece',
    }


def test_parse_without_comment():
    result = parser.parse('Some thing without comments')
    assert result == None


def test_parse_should_only_parse_first():
    result = parser.parse(comment_seed_multiple)
    assert result == {
        'description': 'Hold servo arm to the leg',
        'category': 'leg',
        'type': 'piece',
    }


def test_findComment_should_find():
    expected = ''' * LegPieceA
 * @description Hold servo arm to the leg
 * @category leg
 * @tag piece
 * @stl'''
    result = parser.findComment(comment_seed)
    assert result == expected


def test_findComment_should_not_find():
    result = parser.findComment('doesnt contains any expected comments')
    assert result == None


def test_parseValue_should_parse():
    result = parser.parseValueLine(
        'description',
        ' * @description Lorem ipsum sic amet dolorit'
    )
    assert result == 'Lorem ipsum sic amet dolorit'


def test_lineBreakNormalize_should_normalize():
    result = parser.lineBreakNormalize(r'toto, \r\n tata, \r\n titi \n')
    assert result == r'toto, \n tata, \n titi \n'


def test_extractValueNameFromLine_should_parse():
    result = parser.extractValueNameFromLine(
        ' * @description Lorem ipsum sic amet dolorit'
    )
    assert result == 'description'

def test_extractValueNameFromLineWithoutValue():
    result = parser.extractValueNameFromLine(
        ' * @stl'
    )
    assert result == 'stl'


def test_with_mock(monkeypatch):
    def mock_findComment(*args, **kwargs):
        assert args[0] == comment_seed
        return args[0]

    monkeypatch.setattr(parser, "findComment", mock_findComment)
    result = parser.parse(comment_seed)
    assert result == {
        'description': 'Hold servo arm to the leg',
        'category': 'leg',
        'tag': 'piece',
        'stl': ''
    }
