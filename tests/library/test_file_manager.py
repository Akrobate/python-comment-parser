import os

from src.library import file_manager

dir_path = os.path.dirname(os.path.realpath(__file__))
seeds_example_project_path = os.path.realpath(dir_path + '/../seeds/example_project')
seeds_example_yaml_files = os.path.realpath(dir_path + '/../seeds/example_yaml_files')
write_foler = os.path.realpath(dir_path + '/../seeds/write_folder')

def test_listAllFiles():
    # file_manager
    result = file_manager.listAllFiles(seeds_example_project_path, 'scad')
    expected = [
        seeds_example_project_path + '/element.scad',
        seeds_example_project_path + '/main.scad',
        seeds_example_project_path + '/envelopes/screw-envelope.scad',
        seeds_example_project_path + '/envelopes/servo-arm-envelope.scad',
        seeds_example_project_path + '/envelopes/servo-envelope.scad',
        seeds_example_project_path + '/components/hip.scad',
        seeds_example_project_path + '/components/servo-motor.scad',
        seeds_example_project_path + '/components/leg.scad',
        seeds_example_project_path + '/pieces/leg-piece-d.scad',
        seeds_example_project_path + '/pieces/leg-piece-b.scad',
        seeds_example_project_path + '/pieces/hip-piece-a.scad',
        seeds_example_project_path + '/pieces/hip-piece-c.scad',
        seeds_example_project_path + '/pieces/leg-piece-a.scad',
        seeds_example_project_path + '/pieces/hip-piece-b.scad',
        seeds_example_project_path + '/pieces/leg-piece-c.scad',
        seeds_example_project_path + '/pieces/hip-piece-d.scad',
        seeds_example_project_path + '/pieces/subpieces/articulation-axis-screw.scad',
        seeds_example_project_path + '/pieces/subpieces/articulation-axis.scad',
        seeds_example_project_path + '/assets/screw/screw.scad'
    ]

    assert len(result) == len(expected)

    for expected_item in expected:
        assert expected_item in result
    

def test_generateFileNameFromPath_shoud_format_without_changing_ext():
    result = file_manager.generateFileNameFromPath(
        'lorem/ipsum/sic/amet'
    )
    assert result == 'lorem_ipsum_sic_amet'


def test_generateFileNameFromPath_shoud_format_with_changing_ext():
    result = file_manager.generateFileNameFromPath(
        'lorem/ipsum/sic/amet.scad',
        'scad',
        'png'
    )
    assert result == 'lorem_ipsum_sic_amet.png'


def test_formatProjectFilePath():
    result = file_manager.formatProjectFilePath(
        '/a/b/c/d',
        '/a/b'
    )
    assert result == 'c/d'
    result = file_manager.formatProjectFilePath(
        '/a/b/c/d',
        '/a/b/'
    )
    assert result == 'c/d'


def test_generateYamlString_simplecase():
    seed = {
        'property_a': 1,
        'property_b': 2,
        'property_c': 'String',
    }
    result = file_manager.generateYamlString(seed)
    assert result == 'property_a: 1\nproperty_b: 2\nproperty_c: String\n'


def test_generateYamlString_list_dicts():
    seed = {
        'property_a': 1,
        'property_c': [
            {
                'name': 'element1',
                'element_prop': 2,
            },
            {
                'name': 'element1',
                'element_prop': 2,
            }
        ],
    }
    result = file_manager.generateYamlString(seed)
    expected = 'property_a: 1\nproperty_c:\n- element_prop: 2\n  name: element1\n- element_prop: 2\n  name: element1\n'
    assert result == expected


def test_opendAndParseYamlFile():
    yaml_file_path = seeds_example_yaml_files + '/_config.yml'
    result = file_manager.opendAndParseYamlFile(yaml_file_path)
    
    # asserting presence of some properties
    assert 'title' in result
    assert 'description' in result
    assert 'permalink' in result
    assert 'baseurl' in result
    assert 'url' in result

def test_saveFile():
    test_file_name = 'testing_save_to_file.txt'
    test_path_filename = write_foler + '/' + test_file_name
    
    # preparing folder to be empty of test file
    if os.path.isfile(test_path_filename):
        os.remove(test_path_filename)
    
    string_in_file = 'bla bla bla'
    file_manager.saveFile(string_in_file, test_path_filename)

    file_to_test = open(test_path_filename, 'r', encoding='utf8')
    obtained_string = file_to_test.read()

    assert obtained_string == string_in_file


def test_isAbsolutePath_shoud_be_true():
    file_path = '/absolute/file/path'
    assert file_manager.isAbsolutePath(file_path) == True

    file_path = '/absolute/file/path/filename.txt'
    assert file_manager.isAbsolutePath(file_path) == True

    file_path = '/absolute/file/path/../../filename.txt'
    assert file_manager.isAbsolutePath(file_path) == True


def test_isAbsolutePath_shoud_be_false():
    file_path = 'relative/file/path'
    assert file_manager.isAbsolutePath(file_path) == False

    file_path = './relative/file/path/filename.txt'
    assert file_manager.isAbsolutePath(file_path) == False

    file_path = '../relative/file/path/../../filename.txt'
    assert file_manager.isAbsolutePath(file_path) == False
