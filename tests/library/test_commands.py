from src.library.commands import openscadGenerateRenderImageFile, openscadGenerateStlFile, generateRenderFiles, generateStlFiles, buildJekyllWebsite
import os

def test_generateRenderFiles(monkeypatch):
    def os_sys(*args, **kwargs):
        assert args[0] == "xvfb-run openscad source_openscad_file -o /output/render/path/output_render_file --imgsize=2048,1539 --viewall"

    monkeypatch.setattr(os, "system", os_sys)
    content_data = [
        {
            'file_path': 'source_openscad_file',
            'rendered_file_name': 'output_render_file'
        }
    ]
    generateRenderFiles(content_data, "/output/render/path/")


def test_generateStlFiles(monkeypatch):
    def os_sys(*args, **kwargs):
        assert args[0] == "xvfb-run openscad source_openscad_file -o /output/render/path/output_render_file"

    monkeypatch.setattr(os, "system", os_sys)
    content_data = [
        {
            'file_path': 'source_openscad_file',
            'stl_file_name': 'output_render_file',
            'params': {
                'stl': 1,
            }
        }
    ]
    generateStlFiles(content_data, "/output/render/path/")


def test_openscadGenerateRenderImageFile(monkeypatch):
    def os_sys(*args, **kwargs):
        assert args[0] == "xvfb-run openscad source_openscad_file -o output_render_file --imgsize=2048,1539 --viewall"

    monkeypatch.setattr(os, "system", os_sys)
    openscadGenerateRenderImageFile("source_openscad_file", "output_render_file")


def test_openscadGenerateStlFile(monkeypatch):
    def os_sys(*args, **kwargs):
        assert args[0] == "xvfb-run openscad source_openscad_file -o output_render_file"

    monkeypatch.setattr(os, "system", os_sys)
    openscadGenerateStlFile("source_openscad_file", "output_render_file")


def test_buildJekyllWebsite(monkeypatch):
    def os_sys(*args, **kwargs):
        assert args[0] == "bundle exec jekyll build -d public/path"

    monkeypatch.setattr(os, "system", os_sys)
    buildJekyllWebsite("public/path")