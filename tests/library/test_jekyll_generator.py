
import os
from src.library import file_manager
from src.library import jekyll_generator

dir_path = os.path.dirname(os.path.realpath(__file__))
seeds_example_project_path = os.path.realpath(dir_path + '/../seeds/example_project')

def test_processJekyllPagesGenerationFromContentData(monkeypatch):

    # structure tree is necessary only in this test, to provide a dynamic test seed
    from src.library import structure_tree
    content = structure_tree.generateContentData(seeds_example_project_path)
    content_with_children = structure_tree.generateContentListWithFirstLevelChildren(content)

    content_with_childer_seed = content_with_children[0:1]

    def mock_saveFile(*args, **kwargs):        
        assert args[1] == '/page/path/test/HipLeg.markdown'
        return True

    monkeypatch.setattr(file_manager, "saveFile", mock_saveFile)

    jekyll_generator.processJekyllPagesGenerationFromContentData(content_with_childer_seed, '/page/path/test')



def test_generateJekyllPageFile(monkeypatch):

    test_path_filename = '/tmp/testing_save_jekyll_file.markdown'

    def mock_saveFile(*args, **kwargs):        
        assert args[0] == '---\nprop: 1\nprop2: 2\n---\nbla bla bla'
        assert args[1] == test_path_filename
        return True

    monkeypatch.setattr(file_manager, "saveFile", mock_saveFile)

    string_in_content = 'bla bla bla'
    test_head_data = {
        'prop': 1,
        'prop2': 2,
    }

    jekyll_generator.generateJekyllPageFile(
        test_head_data,
        string_in_content,
        test_path_filename
    )
