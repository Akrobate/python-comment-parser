import os
from src.library.file_manager import opendAndParseYamlFile, isAbsolutePath

'''
Configuration file should be like:

version: 1.0
paths:
  input_openscad_project_folder: ./tests/seeds/example_project
  output_folder_images: /assets/previews
  output_folder_stl: /assets/stl_files
  output_folder_pages: /jekyll/_pages
behaviour:
  clean_render_images_folder: true
  clean_stl_files_folder: true
  clean_pages_folder: true
'''

def loadConfigurationFile(configuration_file):
    real_configuration_file_path = os.path.realpath(configuration_file)
    project_dir_path = os.path.dirname(real_configuration_file_path)
    configuration = opendAndParseYamlFile(real_configuration_file_path)
    validateConfiguration(configuration)
    configuration = transformPathsToAbsolute(configuration)
    return mergeDefaultConfiguration(configuration)


def validateConfiguration(configuration):
    if (not 'version' in configuration):
        raise NameError('InvalidConfiguration')
    if (not 'paths' in configuration):
        raise NameError('InvalidConfiguration')
    if (not 'input_openscad_project_folder' in configuration['paths']):
        raise NameError('InvalidConfiguration')
    if (not 'output_folder_images' in configuration['paths']):
        raise NameError('InvalidConfiguration')
    if (not 'output_folder_stl' in configuration['paths']):
        raise NameError('InvalidConfiguration')
    if (not 'output_folder_pages' in configuration['paths']):
        raise NameError('InvalidConfiguration')

    if isAbsolutePath(configuration['paths']['input_openscad_project_folder']):
        raise NameError('InvalidPathConfiguration')
    if isAbsolutePath(configuration['paths']['output_folder_images']):
        raise NameError('InvalidPathConfiguration')
    if isAbsolutePath(configuration['paths']['output_folder_stl']):
        raise NameError('InvalidPathConfiguration')
    if isAbsolutePath(configuration['paths']['output_folder_pages']):
        raise NameError('InvalidPathConfiguration')


def mergeDefaultConfiguration(configuration):

    default_behaviour = getDefaultBehaviourConfiguration()
    if (not 'behaviour' in configuration):
        configuration['behaviour'] = default_behaviour
    else:
        if (not 'clean_render_images_folder' in configuration['behaviour']):
            configuration['behaviour']['clean_render_images_folder'] = default_behaviour['clean_render_images_folder']
        if (not 'clean_stl_files_folder' in configuration['behaviour']):
            configuration['behaviour']['clean_stl_files_folder'] = default_behaviour['clean_stl_files_folder']
        if (not 'clean_pages_folder' in configuration['behaviour']):
            configuration['behaviour']['clean_pages_folder'] = default_behaviour['clean_pages_folder']
    return configuration


def getDefaultBehaviourConfiguration():
    return {
        'clean_render_images_folder': True,
        'clean_stl_files_folder': True,
        'clean_pages_folder': True,
    }

def transformPathsToAbsolute(configuration):
    return configuration

def getDefaultConfigurationFilename():
    return '.openscad-jekyll.yml'