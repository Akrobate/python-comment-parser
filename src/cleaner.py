import os
from glob import glob
from src.library import file_manager


def clean(configuration):
    # Cleaning previews
    source_path = './data/jekyll/assets/previews'
    extension_filter = 'png'
    file_manager.removeFilesInFolder(source_path, extension_filter)

    # Cleaning stl_files
    source_path = './data/jekyll/assets/stl_files'
    extension_filter = 'stl'
    file_manager.removeFilesInFolder(source_path, extension_filter)

    # Cleaning pages
    source_path = './data/jekyll/_pages'
    extension_filter = 'markdown'
    file_manager.removeFilesInFolder(source_path, extension_filter)


def cleanAll(configuration):

    images_folder = configuration['paths']['output_folder_images']
    stl_folder = configuration['paths']['output_folder_stl']
    pages_folder = configuration['paths']['output_folder_pages']

    # Cleaning previews
    file_manager.removeFilesInFolder(images_folder, 'png')
    file_manager.removeFilesInFolder(stl_folder, 'stl')
    file_manager.removeFilesInFolder(pages_folder, 'markdown')

