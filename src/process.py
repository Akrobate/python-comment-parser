from src.library import structure_tree as st
from src.library import commands as cmd
from src.library import jekyll_generator


def process(configuration):
    # parameters

    debug = False

    if (debug):
        # Will be removed
        data_folder = './data'
        input_openscad_project_folder = './tests/seeds/example_project'
        output_folder_images = data_folder + '/jekyll/assets/previews'
        output_folder_stl = data_folder + '/jekyll/assets/stl_files'
        output_folder_pages = data_folder + '/jekyll/_pages'
    else:
        # Getting data from configuration
        input_openscad_project_folder = configuration['paths']['input_openscad_project_folder']
        output_folder_images = configuration['paths']['output_folder_images']
        output_folder_stl = configuration['paths']['output_folder_stl']
        output_folder_pages = configuration['paths']['output_folder_pages']

    
    # Process flow

    # 1. List all files contents from github source
    content_list = st.generateContentData(input_openscad_project_folder)

    # 2. Render all previews images and store to assets folder
    cmd.generateRenderFiles(content_list, output_folder_images)

    # 3. Generate all 3D models and store to assets folder
    cmd.generateStlFiles(content_list, output_folder_stl)

    # 4. Generete content list with first level chilrden
    content_list_with_first_level_children = st.generateContentListWithFirstLevelChildren(content_list)

    # 5. Generate jekyll md files with related pages and store to _pages
    jekyll_generator.processJekyllPagesGenerationFromContentData(
        content_list_with_first_level_children,
        output_folder_pages
    )

    # 6. configure _config.yml jekyll file
