import re

def parse(content):
    result = {}
    normlized_content = lineBreakNormalize(content)
    comment_content = findComment(normlized_content)
    if comment_content == None:
        return None

    for line in comment_content.split('\n'):
        value_name = extractValueNameFromLine(line)
        if value_name == None:
            continue
        result[value_name] = parseValueLine(value_name, line)
    return result


def findComment(content):
    '''Isolating all test that start with /** and ends with */'''
    # result = re.search(r'/\*\*[\n|\r\n]([.|\r\n|\n|\s|\S]*)[\r\n|\n|\s|\S] \*/', content)
    result = re.search(r'/\*\*[\n|\r\n]([^\*][^/]*)[\r\n|\n|\s|\S] \*/', content)
    return result.group(1) if result else None


def extractValueNameFromLine(line_content):
    '''Returns the @[value name]'''
    result = re.search(r'@(\w*)\s*', line_content)
    return result.group(1) if result else None


def parseValueLine(value_name, line_content):
    '''From a specific comment line returns the value of value_name identified by @'''
    result = re.search(r'^\s\*\s\@' + value_name + r'\s*(.*)$', line_content)
    return result.group(1) if result else None


def lineBreakNormalize(content):
    return content.replace(r'\r\n', r'\n')