from src.library import file_manager
import os

def processJekyllPagesGenerationFromContentData(content_list, pages_path):
    for element in content_list:
        generateJekyllPageFromElementOfContentData(element, pages_path)


def generateJekyllPageFromElementOfContentData(element, pages_path):
    head_data = generateJekyllPageHeadData(element)
    content = wrapStringWithCodeMarkers(file_manager.readFile(element['file_path']))
    page_file_name = element['name'] + '.markdown'
    file_path = os.path.join(pages_path, page_file_name)
    generateJekyllPageFile(head_data, content, file_path)


# Other fields needs to be implemented
def generateJekyllPageHeadData(element):
    head_data = {}

    # Todo: Put it to a specific configuration
    head_data['layout'] = 'page'

    head_data['related-pages'] = generateJekyllRelatedPageList(element)
    head_data['title'] = element['name']
    head_data['description'] = element['params']['description']
    head_data['type'] = element['params']['type']
    head_data['image'] = element['rendered_file_name']
    head_data['stl_link'] = element['stl_file_name']
    return head_data


# Url needs to be implemented
def generateJekyllRelatedPageList(element):
    related_page_list = []
    if 'children' in element:
        for child in element['children']:
            related_page = {}
            related_page['title'] = child['name']
            related_page['image'] = child['rendered_file_name']
            related_page['url'] = child['name']
            related_page['type'] = child['params']['type']
            related_page_list.append(related_page)
    return related_page_list


def generateJekyllPageFile(head_data, content, file_path):
    head_string = file_manager.generateYamlString(head_data)
    output_file_content = '---\n' + head_string + '---\n' + content
    file_manager.saveFile(output_file_content, file_path)

def wrapStringWithCodeMarkers(content):
    return '\n```\n' + content + '\n```\n'
