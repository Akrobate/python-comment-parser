from src.library import parser
from src.library import file_manager
from src.library import commands
import pprint

def generateContentData(project_folder):
    data = []
    path_list = file_manager.listAllFiles(project_folder, 'scad')
    for path in path_list:
        comment_params = {}
        with open(path) as file:
            content = file.read()
            comment_params = parser.parse(content)

        if comment_params != None:
            project_file_path = file_manager.formatProjectFilePath(path, project_folder)
            rendered_file_name = file_manager.generateFileNameFromPath(project_file_path, 'scad', 'png')
            stl_file_name = file_manager.generateFileNameFromPath(project_file_path, 'scad', 'stl')
            data.append({
                'params': comment_params,
                'name': comment_params['name'],
                'file_path': path,
                'project_file_path': project_file_path,
                'rendered_file_name': rendered_file_name,
                'stl_file_name': stl_file_name,
            })
    return data

def generateTree(content_data):
    working_content_list = []
    tree = {
        'children': [],
        'name': 'root'
    }

    # Building finding first level childer
    for item in content_data:
        if 'params' in item and item['params'] != None:
            if not 'parent' in item['params']:
                tree['children'].append(item)
                # working_content_list.remove(item)
            else:
                working_content_list.append(item)

    no_parent_elements = []

    # remove elements without parents
    for item in working_content_list:
        item_has_parent = False

        # searching in working list
        for sub_item in working_content_list:
            if sub_item != item and sub_item['params']['name'] == item['params']['parent']:
                item_has_parent = True

        # searching in tree children list
        for sub_item in tree['children']:
            if sub_item['params']['name'] == item['params']['parent']:
                item_has_parent = True
        
        # remove from working_content_list if no parents
        if (item_has_parent == False):
            no_parent_elements.append(item)
            working_content_list.remove(item)

    # Builds tree emptying working content list
    while len(working_content_list) > 0:
        for item in working_content_list:
            node = findNodeInTree(tree, item['params']['parent'])
            if node != None:
                if 'children' in node:
                    node['children'].append(item)
                else:
                    node['children'] = [ item ]
                working_content_list.remove(item)

    return tree


def findNodeInTree(tree, name):
    def recursiveSearchInTree(branch, name):
        if branch['name'] == name:
            return branch
        elif 'children' in branch:
            for sub_branch in branch['children']:
                rec_res = recursiveSearchInTree(sub_branch, name)
                if (rec_res != None):
                    return rec_res
        else:
            return None
    return recursiveSearchInTree(tree, name)


def generateContentListWithFirstLevelChildren(content_data):
    
    working_content_list = []

    for item in content_data:
        if 'params' in item and item['params'] != None:
            working_content_list.append(item)

    result_list = working_content_list[:]

    for item in result_list:
        # searching in working list
        for sub_item in working_content_list:
            if itemIsParentOfSubItem(item, sub_item):
                if 'children' in item:
                    item['children'].append(sub_item)
                else:
                    item['children'] = [sub_item]

    return result_list


def enrichContentListWithRootProperty(content_data):
    
    working_content_list = content_data[:]

    for index, working_item in enumerate(working_content_list):
        if 'params' in working_item and 'parent' not in working_item['params']:
            working_content_list[index]['root'] = True
        else:
            working_content_list[index]['root'] = False

    return working_content_list


def itemIsParentOfSubItem(item, sub_item):
    return 'parent' in sub_item['params'] and sub_item['params']['parent'] == item['params']['name']
