import yaml
from glob import glob
import os

def listAllFiles(source_path, extension_filter):
    result = glob(source_path + "/**/*." + extension_filter, recursive = True)
    return result


def formatProjectFilePath(path, source_path):
    project_path = path.replace(source_path, '')
    if project_path[0] == '/':
        project_path = project_path.replace('/', '', 1)
    return project_path


def generateFileNameFromPath(path, old_extention = None, new_extention = None):
    filename = path.replace('/', '_')
    if old_extention != None and new_extention != None:
        filename = filename.replace('.' + old_extention, '.' + new_extention)
    return filename


def generateYamlString(data):
    return yaml.dump(data, default_flow_style=False)


def opendAndParseYamlFile(file_path):
    parsed_yaml_data = {}
    parsed_yaml_data = yaml.safe_load(readFile(file_path))
    return parsed_yaml_data


def saveFile(data, file_path):
    with open(file_path, 'w') as outfile:
        outfile.write(data)


def readFile(file_path):
    result = ''
    with open(file_path, 'r', encoding='utf8') as stream:
        result = stream.read()
    return result


def removeFile(file_path):
    os.remove(file_path)


def removeFilesInFolder(path, extension_filter):
    for file_name in listAllFiles(path, extension_filter):
        removeFile(file_name)

def isAbsolutePath(path):
    return os.path.isabs(path)