import os

def generateRenderFiles(content_data, output_path):
    for item in content_data:
        openscadGenerateRenderImageFile(item['file_path'], os.path.join(output_path, item['rendered_file_name']))


def generateStlFiles(content_data, output_path):
    for item in content_data:
        if 'stl' in item['params']:
            openscadGenerateStlFile(item['file_path'], os.path.join(output_path, item['stl_file_name']))


def openscadGenerateRenderImageFile(source_openscad_file, output_render_file):
    return os.system("xvfb-run openscad " + source_openscad_file + " -o " + output_render_file + " --imgsize=2048,1539 --viewall")


def openscadGenerateStlFile(source_openscad_file, output_stl_file):
    return os.system("xvfb-run openscad " + source_openscad_file + " -o " + output_stl_file)

# default path was ../public
def buildJekyllWebsite(path):
    return os.system("bundle exec jekyll build -d " + path)