# FROM debian:bullseye
FROM ubuntu:18.04

RUN apt update
RUN apt-get update

# Python stuff
RUN apt install python3-pip -y
RUN pip3 install pytest
RUN pip3 install coverage
RUN pip3 install pyyaml

# Openscad stuff
RUN apt-get install software-properties-common -y
RUN add-apt-repository ppa:openscad/releases
RUN apt-get install xvfb openscad -y

# Jekyll generator stuff
RUN apt install ruby-full -y
RUN gem install jekyll bundler
RUN apt-get install zlib1g-dev -y

# Install git
RUN apt-get install git -y

# Temporary debug tools
RUN apt-get install vim -y
RUN apt-get install iputils-ping -y

# Configure runner
RUN echo "alias osj='python3 /project/main.py'" >> /root/.bash_aliases


WORKDIR /project
